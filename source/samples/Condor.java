package samples;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import de.aknuth.phantomjs.EventType;
import de.aknuth.phantomjs.JavaBindings;
import de.aknuth.phantomjs.Key;


public class Condor {
	

	public static void main(String[] args) throws Exception {
		Condor condor = new Condor();
		JavaBindings session = new JavaBindings();
		condor.test(session);
	}

	
	public void test(JavaBindings session) throws Exception {
		session.putTimeout(500);
		session.start("http://www.condor.com/ibe/cfi/de/flight/search.xhtml");
		session.sendEvent(EventType.click,"#searchform\\:flightMode\\:1");//
		session.sendEvent(EventType.click,"#searchform\\:originInput");
		session.keypress("DUS");
		session.keypress(Key.Tab);
		session.sendEvent(EventType.click,"#searchform\\:destinationInput");
		session.keypress("LPA");
		session.keypress(Key.Tab);
		session.sendEvent(EventType.click,"#outboundDateInput");
		session.sendEvent(EventType.click,"#LayoutCal0_1cell_17");
		session.sendEvent(EventType.click,"#searchform\\:search");
		byte[] bytes = session.capture();
		FileUtils.writeByteArrayToFile(new File("condor.png"), bytes);
		List<String> out = session.text("table.vacancies");
		for (String s : out){
			System.out.println(StringUtils.strip(s));
		}
		session.exit();
	}
}
