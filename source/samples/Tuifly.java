package samples;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import de.aknuth.phantomjs.EventType;
import de.aknuth.phantomjs.JavaBindings;

public class Tuifly {
	public static void main(String[] args) throws Exception{
		Tuifly tuifly = new Tuifly();
		JavaBindings session = new JavaBindings();
		tuifly.test(session);
	}

	
	public void test(JavaBindings session) throws Exception {
		DateTime dt = new DateTime(2014,9,24,0,0,DateTimeZone.UTC);
		session.putTimeout(300);
		session.start("http://www.tuifly.com/de/index.html");
		session.sendEvent(EventType.mousedown,".tfl-checkbox");
		session.sendEvent(EventType.mouseup,".tfl-checkbox");
//		session.sendEvent(EventType.click,".tfl-checkbox");
//		session.keypress("\t");
//		session.sendEvent(EventType.mousedown,".js-airport-input");
//		session.sendEvent(EventType.mouseup,".js-airport-input");
		session.sendEvent(EventType.doubleclick,".js-airport-input");
//		session.sendEvent(EventType.mouseup,".js-airport-input");
//		session.putTimeout(500);
		session.keypress("DUS");
//		session.keypress("\t");
//		session.sendEvent(EventType.mousedown,"input[data-type='destination']");
//		session.sendEvent(EventType.mouseup,"input[data-type='destination']");
		session.sendEvent(EventType.doubleclick,"input[data-type='destination']");
		session.keypress("LPA");
//		session.keypress("\t");
////		session.keypress("\t");
////		session.keypress("\t");
//		session.sendEvent(EventType.mousedown,"input[data-type='start']");
//		session.sendEvent(EventType.mouseup,"input[data-type='start']");
		session.sendEvent(EventType.doubleclick,"input[data-type='start']");
		session.sendEvent(EventType.click,"td[data-timestamp='"+dt.toDate().getTime()+"']");
		
//		session.keypress("13.09.2014");
////		Thread.sleep(1000);
//		session.keypress("\t");
////		session.click("#NavAngebote a");
		//session.sendClickEvent(EventType.click,"110","520");
		session.sendEvent(EventType.click, "button.big");
		
		byte[] bytes = session.capture();
		FileUtils.writeByteArrayToFile(new File("tuifly.png"), bytes);

		session.exit();
	}
}
