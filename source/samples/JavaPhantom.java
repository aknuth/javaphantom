package samples;

import phantom.JavaCallback;
import phantom.PhantomJava;
import phantom.QApplication;

public class JavaPhantom extends JavaCallback {
	private PhantomJava phantom;
	
	/**
	 * Constructor - initialize Phantom
	 */
	public JavaPhantom() {
		String[] args = {};
		QApplication qapp = new QApplication(args);

		phantom = PhantomJava.instance();
		
		phantom.listen(this);

		// TODO: anpassen!
		phantom.config().setScriptFile("scripts/simplebindings.js");
		String[] scriptArgs = {"foo", "bar", "baz"};
        phantom.config().setScriptArgs(scriptArgs);
        
		// execute top-level code synchronously
		if (phantom.execute()) {
			// wait for async code to finish / phantom to exit
			QApplication.exec();
		}

		// script execution ended
		System.out.println("Done: " + (phantom.returnValue() != 0 ? "Failure!" : "Success!"));

		// delete the phantom singleton and Qt application
		phantom.delete();
		qapp.delete();
	}
	
	/**
	 * callback from javascript
	 * handles start,exit,command callback
	 */
	@Override
	public void phantomJsCallback(String data) {
		System.out.println("Got message from PhantomJS: " + data);
        phantom.call("foo!!");
        phantom.exit();
	}
	
	public static void main(String[] args) {
		JavaPhantom b = new JavaPhantom();
	}
}
