package samples;

import java.io.File;

import org.apache.commons.io.FileUtils;

import de.aknuth.phantomjs.JavaBindings;

public class Skyscanner {
	public static void main(String[] args) throws Exception {
		Skyscanner skyscanner = new Skyscanner();
		JavaBindings session = new JavaBindings();
		skyscanner.test(session);
//		skyscanner.test1(session);
	}

	
	private void test1(JavaBindings session) throws Exception {
		session.start("http://www.skyscanner.de/transport/fluge/duss/lpa/141008/flugpreise-von-dusseldorf-nach-gran-canaria-las-palmas-im-august-2014.html?rtn=0");
		while (session.test("$jq('div.day-progress-bar').is(':visible')")){
			System.err.println("Sleeping");
			Thread.sleep(1000);
		}
		byte[] bytes = session.capture();
		FileUtils.writeByteArrayToFile(new File("skyscanner.png"), bytes);
		session.exit();
	}


	public void test(JavaBindings session) throws Exception {
		session.start("http://www.skyscanner.de/transport/fluge/duss/lpa/141003/flugpreise-von-dusseldorf-nach-gran-canaria-las-palmas-im-august-2014.html?rtn=0");
		session.waitFor("!$jq('div.day-progress-bar').is(':visible')",30000);
		byte[] bytes = session.capture();
		FileUtils.writeByteArrayToFile(new File("skyscanner.png"), bytes);
		
		session.exit();
	}
}
