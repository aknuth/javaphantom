package de.aknuth.phantomjs;

import javax.json.JsonObject;

import org.apache.commons.codec.binary.Base64;

public class CaptureResultHandler<T> implements ResultHandler<byte[]> {
	
	private JsonObject jobject;
	public CaptureResultHandler(JsonObject jobject){
		this.jobject=jobject;
	}
	@Override
	public byte[] handle() {
		return Base64.decodeBase64(jobject.getString("capture"));
	}

}
