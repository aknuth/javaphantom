package de.aknuth.phantomjs;

import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

public class ResultFactory {
	
	private static ResultFactory factory = new ResultFactory();
	
	private ResultFactory(){}
	
	public static ResultFactory get(){
		return factory;
	}
	
	public <T extends ResultHandler<?>> ResultHandler<?> getResultHandler(JsonObject jobject){
		Long result = 0l;
		if (jobject != null && jobject.get("result") != null){
			result = ((JsonNumber)jobject.get("result")).longValue();
			if (jobject.get("capture")!=null){
				return new CaptureResultHandler<byte[]>(jobject);
			} else if (jobject.get("text")!=null){
				return new TextResultHandler<List<String>>(jobject);
			} else if (jobject.get("test")!=null){
				return new TestResultHandler<Boolean>(jobject);
			} else if (result == -1){
				return new ExitHandler<Boolean>(result);
			} else {
				return new NoResultHandler<Boolean>(jobject);
			}
		} else {
			return null;
		}
	}
	
	public static void navigateTree(JsonValue tree, String key) {
		   if (key != null)
		      System.out.print("Key " + key + ": ");
		   switch(tree.getValueType()) {
		      case OBJECT:
		         System.out.println("OBJECT");
		         JsonObject object = (JsonObject) tree;
		         for (String name : object.keySet())
		            navigateTree(object.get(name), name);
		         break;
		      case ARRAY:
		         System.out.println("ARRAY");
		         JsonArray array = (JsonArray) tree;
		         for (JsonValue val : array)
		            navigateTree(val, null);
		         break;
		      case STRING:
		         JsonString st = (JsonString) tree;
		         System.out.println("STRING " + st.getString());
		         break;
		      case NUMBER:
		         JsonNumber num = (JsonNumber) tree;
		         System.out.println("NUMBER " + num.toString());
		         break;
		      case TRUE:
		      case FALSE:
		      case NULL:
		         System.out.println(tree.getValueType().toString());
		         break;
		   }
		}	
	
}
