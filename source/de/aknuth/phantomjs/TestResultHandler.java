package de.aknuth.phantomjs;

import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

public class TestResultHandler<S> implements ResultHandler<Object> {
	private JsonObject jobj;
	
	public TestResultHandler(JsonObject jobject){
		this.jobj=jobject;
	}
	@Override
	public Object handle() {
		return resolveResult(jobj.get("test"));
	}

	
	public Object resolveResult(JsonValue jvalue) {
		   Object result = null;
		   switch(jvalue.getValueType()) {
		      case OBJECT:
//		         System.out.println("OBJECT");
//		         JsonObject object = (JsonObject) jobject;
//		         for (String name : object.keySet())
//		            navigateTree(object.get(name), name);
//		         break;
		    	 throw new IllegalArgumentException("not supported ...");
		      case ARRAY:
//		         System.out.println("ARRAY");
//		         JsonArray array = (JsonArray) jobject;
//		         for (JsonValue val : array)
//		            navigateTree(val, null);
//		         break;
		    	 throw new IllegalArgumentException("not supported ...");
		      case STRING:
		         JsonString st = (JsonString) jvalue;
		         result = st.getString();
		         break;
		      case NUMBER:
		         JsonNumber num = (JsonNumber) jvalue;
		         result = num.longValue();
		         break;
		      case TRUE: result = Boolean.TRUE; break;
		      case FALSE: result = Boolean.FALSE;break;
		      case NULL: result = null;
		      //case DEFAULT: return null;
		   }
		   return result;
		}	
}
