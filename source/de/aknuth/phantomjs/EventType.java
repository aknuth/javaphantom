package de.aknuth.phantomjs;

public enum EventType {
	mouseup, mousedown, mousemove, doubleclick, click, keyup, keypress, keydown;
}
