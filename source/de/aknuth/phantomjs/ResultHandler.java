package de.aknuth.phantomjs;

public interface ResultHandler<T> {
	public T handle();
}
