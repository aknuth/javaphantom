package de.aknuth.phantomjs;

import java.io.StringReader;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.stream.JsonParser;

import org.apache.log4j.Logger;

import phantom.JavaCallback;
import phantom.PhantomJava;
import phantom.QApplication;

public class PhantomInitializer<T extends Object> extends JavaCallback implements Runnable{
	
	private static Logger log = Logger.getLogger("trace." + JavaBindings.class.getName());
	PhantomJava phantom;
	private JavaBindings jb;
	ResultHandler<?> resultHandler;
	
	public PhantomInitializer(JavaBindings jb){
		this.jb=jb;
	}
	@Override
	public void run() {
		String[] args = {};
		QApplication qapp = new QApplication(args);

		phantom = PhantomJava.instance();
		
		phantom.listen(this);
		phantom.config().setPrintDebugMessages(false);

		// TODO: anpassen!
		phantom.config().setScriptFile("scripts/javabinding.js");
		String[] scriptArgs = { "start" };
		phantom.config().setScriptArgs(scriptArgs);
		
		// execute top-level code synchronously
		if (phantom.execute()) {
			// wait for async code to finish / phantom to exit
			QApplication.exec();
		}

		// script execution ended
		log.debug("Done: " + (phantom.returnValue() != 0 ? "Failure!" : "Success!"));

		try {
			jb.lock.lock();
			jb.condition.signal();
		} finally {
			jb.lock.unlock();
		}

		// delete the phantom singleton and Qt application
		phantom.delete();
		qapp.delete();
	}
	
	/**
	 * callback from javascript
	 * handles start,exit,command callback
	 */
	@Override
	public void phantomJsCallback(String data) {
//		Map<String,?> map = (Map)JSONValue.parse(data);
		JsonReader reader = Json.createReader(new StringReader(data));
		JsonStructure jsonStructure = reader.read();
		if (jsonStructure instanceof JsonObject){
			JsonObject jobject = (JsonObject)jsonStructure;
			this.resultHandler = ResultFactory.get().getResultHandler(jobject);
			if (resultHandler instanceof ExitHandler){
				((ExitHandler)resultHandler).exit(phantom);
			}
		}
		try {
			jb.lock.lock();
			jb.condition.signal();
		} finally {
			jb.lock.unlock();
		}
	}
	
}
