package de.aknuth.phantomjs;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

public class TextResultHandler<S> implements ResultHandler<List<String>> {
	private JsonObject jobject;
	
	public TextResultHandler(JsonObject jobject){
		this.jobject=jobject;
	}
	@Override
	public List<String> handle() {
		List<String> result = new ArrayList<String>();
		for (JsonValue jv :(JsonArray)jobject.get("text")){
			result.add(jv.toString());
		}
		return result;
	}

}
