package de.aknuth.phantomjs;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class JavaBindings {
	private static int DEFAULT_TIMEOUT = 50;
	
	private static Logger log = Logger.getLogger("trace." + JavaBindings.class.getName());	
	Lock lock;
	Condition condition;
	private PhantomInitializer pi;
	private int timeout = DEFAULT_TIMEOUT;
	
	/**
	 * Constructor - initialize Phantom
	 */
	public JavaBindings() {
		pi = new PhantomInitializer(this);
		new Thread(pi).start();
	}
	

	private <T> T call(String jsonCommand){
		log.debug("execute phantom call --> "+jsonCommand);
		if (lock==null){
			lock = new ReentrantLock();
			condition = lock.newCondition();
			lockwait();
		}
		pi.phantom.call(jsonCommand);
		lockwait();
		return (T)pi.resultHandler.handle();
	}
	
	private void lockwait(){
		try {
			lock.lock();
			condition.await();
		} catch (InterruptedException e){
		} finally {
			lock.unlock();
		}
	}
	
	public byte[] capture(){
		return call(createCommand("capture"));
	}
	public List<String> text(String selector){
		String s = Base64.encodeBase64String(selector.getBytes());
		return call(createCommand("text",s));
	}
	public Boolean test(String function){
		String f = Base64.encodeBase64String(function.getBytes());
		return call(createCommand("test",f));
	}
	public Object execute(String function){
		String f = Base64.encodeBase64String(function.getBytes());
		return call(createCommand("test",f));
	}
	public void waitFor(String condition, int... timeout) {
		String f = Base64.encodeBase64String(condition.getBytes());
		if (timeout.length>0){
			call(createCommand("waitFor",f, Integer.toString(timeout[0])));
		} else {
			call(createCommand("waitFor",f));
		}
	}
	
	public void setContent(String html){
		String s = Base64.encodeBase64String(html.getBytes());
		call(createCommand("content",s));
	}
	
	public void start(String url){
		call(createCommand("start",url));
	}
	
	public void click(String selector){
		String s = Base64.encodeBase64String(selector.getBytes());
		call(createCommand("click",s));
	}
	
	public void switchToFrame(String framename){
		call(createCommand("frame",framename));
	}
	
	public void exit(){
		call(createCommand("exit"));
	}
	
	public void keypress(String keys){
		String s = Base64.encodeBase64String(keys.getBytes());
		call(createCommand("keypress",s));
	}
	public void keypress(Key k){
		String s = Base64.encodeBase64String(k.toString().getBytes());
		call(createCommand("singleKeypress",s));
	}
	public void sendEvent(EventType type, String selector){
		String s = Base64.encodeBase64String(selector.getBytes());
		call(createCommand("sendEvent",type.toString(),s));
	}
	public void sendClickEvent(EventType type, String x, String y){
		call(createCommand("sendClickEvent",type.toString(),x,y));
	}
	public byte[] pdf(){
		return call(createCommand("pdf"));
	}
	public void putTimeout(int timeout){
		this.timeout = timeout;
	}
	private String createCommand(final String command, final String... parameters) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		JsonObjectBuilder sbuilder = Json.createObjectBuilder();
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (String parameter : parameters) {
			arrayBuilder.add(parameter);
		}
		sbuilder.add(command, arrayBuilder);
		builder.add("command", sbuilder);
		if (this.timeout>0){
			builder.add("timeout", timeout);
		}
		return builder.build().toString();
	}

}
