package de.aknuth.phantomjs;

import javax.json.JsonObject;

public class NoResultHandler<T> implements ResultHandler<Boolean> {
	
	private JsonObject jobject;
	
	public NoResultHandler(JsonObject jobject){
		this.jobject=jobject;
	}
	@Override
	public Boolean handle() {
		if (jobject.getInt("result")==0){
			return true;
		} else {
			return false;
		}
	}

}
