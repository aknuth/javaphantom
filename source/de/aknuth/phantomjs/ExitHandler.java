package de.aknuth.phantomjs;

import phantom.PhantomJava;

public class ExitHandler<T> implements ResultHandler<Boolean> {
	
	private Long result;

	public ExitHandler(Long result){
		this.result=result;
	}
	
	@Override
	public Boolean handle() {
		return true;
	}
	
	public void exit(PhantomJava phantom){
		phantom.exit();
	}
}
